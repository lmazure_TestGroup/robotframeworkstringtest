# RobotFrameworkStringTest

Simplistic Robot Framework tests to play with OTF (see https://autom-devops-en.doc.squashtest.com/latest/autom/techno/robotframework.html)


The supported actions are:
- Given
    - "Variable {varName} is set to {value}"
    - "Variable {varName} is set to Squash parameter {paramName}"
    - "Variable {varName} is set to environment variable {varEnvName}"

- When
    - "Variable {varName} is reverted"
    - "Variable {varName} is uppercased"
    - "Variable {varName} is the concatenation of variable {varName} and variable {varName} joined with {value}"
    - "Variable {varName} is the concatenation of variable {varName}, variable {varName}, and variable {varName} joined with {value}"
    - "Variable {varName} is the concatenation of variable {varName}, variable {varName}, variable {varName}, and variable {varName} joined with {value}"

- Then
    - "Variables {varName} and {varName} are equal"
    - "Variable {varName} is equal to {value}"    
