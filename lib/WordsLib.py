import robot.api.logger
import robot.utils.asserts


allStrings = {}

def set_string_to(var, value):
    allStrings[var] = value

def revert_string(var):
    allStrings[var] = allStrings[var][::-1]

def uppercase_string(var):
    allStrings[var] = allStrings[var].upper()

def concatenate_2_strings(var, var1, var2, separator):
    allStrings[var] = allStrings[var1] + separator + allStrings[var2]

def concatenate_3_strings(var, var1, var2, var3, separator):
    allStrings[var] = allStrings[var1] + separator + allStrings[var2] + separator + allStrings[var3]

def concatenate_4_strings(var, var1, var2, var3, var4, separator):
    allStrings[var] = allStrings[var1] + separator + allStrings[var2] + separator + allStrings[var3] + separator + allStrings[var4]

def assert_strings_are_equal(var1, var2):
    robot.utils.asserts.assert_equal(allStrings[var1], allStrings[var2])

def assert_string_is_equal_to(var, value):
    robot.utils.asserts.assert_equal(allStrings[var], value)
