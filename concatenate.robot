*** Settings ***
Documentation    Variables are concatenable
Resource	my_keywords.resource

*** Test Cases ***

Variables can be concatenated
    Given Variable "a" is set to "α alpha"
    And Variable "b" is set to "β beta"
    And Variable "c" is set to "γ gamma"
    And Variable "d" is set to "δ delta"
    When Variable "ab" is the concatenation of variable "a" and variable "b" joined with "-"
    And Variable "abc" is the concatenation of variable "a", variable "b", and variable "c" joined with " "
    And Variable "abcd" is the concatenation of variable "a", variable "b", variable "c", and variable "d" joined with "👩‍❤️‍💋‍👨"
    Then Variable "ab" is equal to "α alpha-β beta"
    And Variable "abc" is equal to "α alpha β beta γ gamma"
    And Variable "abcd" is equal to "α alpha👩‍❤️‍💋‍👨β beta👩‍❤️‍💋‍👨γ gamma👩‍❤️‍💋‍👨δ delta"
