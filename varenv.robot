*** Settings ***
Documentation    Environment variables are retrievable
Resource	my_keywords.resource

*** Test Cases ***

An environment variable can be retrieved
    When Variable "a" is set to environment variable "SYSTEMDRIVE"
    Then Variable "a" is equal to "C:"
