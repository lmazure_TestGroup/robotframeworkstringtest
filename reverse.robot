*** Settings ***
Documentation    Variables are revertable
Resource	my_keywords.resource

*** Test Cases ***

A variable can be inverted
    When Variable "a" is set to "azerty"
    when Variable "b" is set to "ytreza"
    When Variable "a" is reverted
    Then Variables "a" and "b" are equal
