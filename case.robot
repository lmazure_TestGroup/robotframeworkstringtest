*** Settings ***
Documentation    Variables are uppercasable
Resource	my_keywords.resource

*** Test Cases ***

A variable can be uppercased
    Given Variable "a" is set to "azerty"
    And Variable "b" is set to "AZERTY"
    When Variable "a" is uppercased
    Then Variables "a" and "b" are equal
